A partir de l'esquema de xarxes i ips de l'aula:


                                                    +------------ internet
                                                    |
                                                +-------+
                       +------------------------+ PROFE +---------------------+
                       |                        +-------+                     |
      2.1.1.0/24       |.254                                 2.1.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.6        |.5        |.4                              |.3        |.2        |.1
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC06 |    |PC05 |    |PC04 |                          |PC03 |    |PC02 |    |PC01 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.2.1.0/24       |.254                                 2.2.2.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.12       |.11       |.10                             |.9        |.8        |.7
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC12 |    |PC11 |    |PC10 |                          |PC09 |    |PC08 |    |PC07 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.3.1.0/24       |.254                                 2.2.3.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.18       |.17       |.16                             |.15       |.14       |.13
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC18 |    |PC17 |    |PC16 |                          |PC15 |    |PC14 |    |PC13 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.4.1.0/24       |.254                                 2.2.4.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.24       |.23       |.22                             |.21       |.20       |.19
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC24 |    |PC23 |    |PC22 |                          |PC21 |    |PC20 |    |PC19 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
                       |                                                      |
      2.5.1.0/24       |.254                                 2.2.5.0/24       |.254
      +-----------------------------------+                  +-----------------------------------+
            |.30       |.29       |.28                             |.27       |.26       |.25
            |          |          |                                |          |          |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         |PC30 |    |PC29 |    |PC28 |                          |PC27 |    |PC26 |    |PC25 |
         +-----+    +-----+    +-----+                          +-----+    +-----+    +-----+
         
###### 0. Identifica quin és el teu PC, que ips/màscares ha de portar sobre quines interfícies
pc-05 --->2.1.1.5 //

###### 1. Llista d'ordres per poder introduir manualment les adreces ip del teu lloc de treball amb l'ordre ip i evitar que el NetworkManager o configuracions prèvies interfereixin en aquestes configuracions

systemctl stop NetworkManager
dhclient -r
ip a f dev enp2s0
ip r f all #route flush
ip a a 2.1.1.5/24 dev enp2s0
ip r a default via (2.1.1.1/24)
ip a a 2.1.1.5/24 dev enp2s0


###### 2. Llista d'ordres per configurar el PC del mig de la teva fila com a router amb un comentari explicant el que fa cada línia

systemctl stop NetworkManager
dhclient -r
ip a f dev enp2s0
ip r f all #route flush
ip a a 2.1.1.5/24 dev enp2s0
ip r a default via
ip a a 2.2.1.11/24 dev enp2s0
ip a a PROFE dev enp2s0


###### 3. Explicació de cada línia de la sortida del comando ip route xou

[isx46995096@j05 ~]$ ip route show
default via 192.168.0.5 dev enp2s0  proto static  metric 100 
192.168.0.0/16 dev enp2s0  proto kernel  scope link  src 192.168.3.5  metric 100 




###### 4. Fer un ping a un pc de la teva mateixa fila i al PC19 i al PC24. Capturar els paquets amb wireshark i guardar la captura. Analitzar-la observant el camp TTL del protocol IP. Explicar els diferents valors d'aquest camp en funció de la ip a la qual s'ha fet el ping

 A la meva fila: ping 2.1.1.4
 Pc19 : ping 2.2.4.19
 Pc24: ping 2.5.1.24
 

###### 5. Fer un traceroute i un mtr al 8.8.8.8 Explicar a partir de que ip es talla i per què.

traceroute 8.8.8.8


$ mtr -n --report 8.8.8.8
HOST: j05.informatica.escoladeltr Loss%   Snt   Last   Avg  Best  Wrst StDev
  1.|-- 192.168.0.5                0.0%    10    0.5   0.4   0.4   0.5   0.0
  2.|-- 10.1.1.199                 0.0%    10    2.5   3.0   2.3   6.1   0.9
  3.|-- 10.10.1.4                  0.0%    10    0.7   0.6   0.5   0.8   0.0
  4.|-- 80.58.67.115               0.0%    10    1.7   2.0   1.4   2.5   0.0
  5.|-- 80.58.80.77                0.0%    10   13.6  13.7  10.6  20.3   3.3
  6.|-- 80.58.106.1                0.0%    10   11.5  12.7   9.5  21.0   3.1
  7.|-- ???                       100.0    10    0.0   0.0   0.0   0.0   0.0
  8.|-- 5.53.1.74                  0.0%    10   11.6  11.5  10.6  11.9   0.0
  9.|-- 72.14.233.161              0.0%    10   10.5  10.5   9.7  10.8   0.0
 10.|-- 216.239.48.245             0.0%    10   12.6  12.3  10.9  16.1   1.6
 11.|-- 8.8.8.8                    0.0%    10   10.5  11.2  10.2  12.7   0.5

Es talla a partir del numero 7, on oerdem el 100% dels paquets ja que fa un salt de conexió (en el temps de WRST veiem com va augmentant el temps)



###### 6. Explica que és l'emmascarament i com s'aplicaria en l'ordinador del profe per donar sortida a internet


L'enmascarament de la direcció IP es el servidor que conecta els equips interns cap a la "xarxa externa", internet en aquest cas.
