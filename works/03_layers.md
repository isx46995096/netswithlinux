#### 1. ip a

	Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo.
	 Comprueba que no queda ninguna con "ip a" y "ip r"
	 
	 
	 ip r f all
	 ip a f dev enp2s0
	 
	 
	 ip a
	 
	 # ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:b6:e0:1b brd ff:ff:ff:ff:ff:ff
	3: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	 
		 ip r

	Ponte las siguientes ips en tu tarjeta ethernet: 
	2.2.2.2/24, 
	3.3.3.3/16, 
	4.4.4.4/25
	
	[root@j05 ~]# ip a a 2.2.2.2/24 dev enp2s0
	[root@j05 ~]# ip a a 3.3.3.3/16 dev enp2s0
	[root@j05 ~]# ip a a 4.4.4.4/25 dev enp2s0


	Consulta la tabla de rutas de tu equipo
	
	[root@j05 ~]# ip r
	2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4 


	Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

	2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132
	
		# ping 2.2.2.2
	PING 2.2.2.2 (2.2.2.2) 56(84) bytes of data.
	64 bytes from 2.2.2.2: icmp_seq=1 ttl=64 time=0.035 ms
	64 bytes from 2.2.2.2: icmp_seq=2 ttl=64 time=0.033 ms
	64 bytes from 2.2.2.2: icmp_seq=3 ttl=64 time=0.063 ms
	64 bytes from 2.2.2.2: icmp_seq=4 ttl=64 time=0.062 ms
	^C
	--- 2.2.2.2 ping statistics ---
	4 packets transmitted, 4 received, 0% packet loss, time 2999ms
	rtt min/avg/max/mdev = 0.033/0.048/0.063/0.015 ms
	
	
		# ping 2.2.2.254    /2.2.5.2  /3.3.3.35  / 3.3.200.45   /4.4.4.8
	PING 2.2.2.254 (2.2.2.254) 56(84) bytes of data.
	From 2.2.2.2 icmp_seq=1 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=2 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=3 Destination Host Unreachable
	From 2.2.2.2 icmp_seq=4 Destination Host Unreachable
	^C
	--- 2.2.2.254 ping statistics ---
	4 packets transmitted, 0 received, +4 errors, 100% packet loss, time 2999ms
	pipe 4
	
	
		[root@j05 ~]# ping 2.2.5.2  / ping 4.4.4.132
	connect: Network is unreachable



#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	# ip r f all
	# ip a f dev enp2s0

Conecta una segunda interfaz de red por el puerto usb

	usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff


Cambiale el nombre a usb0

	[root@j05 ~]# ip link set usb0 down
	usb0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff

	[root@j05 ~]# ip link set usb0 name usb0xarxes
	[root@j05 ~]# ip link set usb0xarxes up
	[root@j05 ~]# ip a
	
	4: usb0xarxes: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff


	

Modifica la dirección MAC

	ip link set usb0xarxes down
	4: usb0xarxes: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff

	ip link set usb0xarxes address 00:11:22:33:44:55
	ip link set usb0xarxes up
	
	 usb0xarxes: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff





Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.

	ip a a 5.5.5.5724 dev usb0xarxes
	ip a a 7.7.7.7/24 dev enp2s0

	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:b6:e0:1b brd ff:ff:ff:ff:ff:ff
		inet 192.168.3.5/16 brd 192.168.255.255 scope global dynamic enp2s0
		   valid_lft 21066sec preferred_lft 21066sec
		inet 7.7.7.7/24 scope global enp2s0
		   valid_lft forever preferred_lft forever
	4: usb0xarxes: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:11:22:33:44:55 brd ff:ff:ff:ff:ff:ff
		inet 5.5.5.5/24 scope global usb0xarxes
		   valid_lft forever preferred_lft forever


Observa la tabla de rutas

	#ip r
	default via 192.168.0.5 dev enp2s0  proto static  metric 100 
	5.5.5.0/24 dev usb0xarxes  proto kernel  scope link  src 5.5.5.5 linkdown 
	7.7.7.0/24 dev enp2s0  proto kernel  scope link  src 7.7.7.7 
	192.168.0.0/16 dev enp2s0  proto kernel  scope link  src 192.168.3.5 
	192.168.0.0/16 dev enp2s0  proto kernel  scope link  src 192.168.3.5  metric 100 




#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	 ip a f dev enp2s0



En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

	ip a a 172.16.99.05/24 dev enp2s0

	enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:b6:e0:1b brd ff:ff:ff:ff:ff:ff
    inet 172.16.99.5/24 scope global enp2s0
       valid_lft forever preferred_lft forever

Lanzar iperf en modo servidor en cada ordenador

[root@j05 netswithlinux]# iperf -s 172.66.99.04/24
iperf: ignoring extra argument -- 172.66.99.04/24
------------------------------------------------------------
Server listening on TCP port 5001
TCP window size: 85.3 KByte (default)
------------------------------------------------------------



Comprueba con netstat en qué puerto escucha

Conectarse desde otro pc como cliente

Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

Encontrar los 3 paquetes del handshake de tcp

Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas

