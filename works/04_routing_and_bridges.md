BRIDGES Y ROUTERS

##### 1. Conexionado, nombres de interfaces y direcciones MAC

Para esta práctica hay que interconectar los equipos de una fila con 2 latiguillos de red (sin usar las rosetas del aula) siguiendo el siguiente esquema:



```
FILA DE 3 EQUIPOS:
                              |usbB                          
   +--------+            +--------+            +--------+   
   |        |eth0    usbA|        |usbC    eth0|        |   
   | HOST A +------------+ HOST B +------------+ HOST C |   
   |        |            |        |            |        |   
   +--------+            +--------+            +--------+   
                             |eth0                          
```

El equipo del medio de la fila ha de tener conectadas 3 tarjetas usb-ethernet. Se han de renombrar estas tarjetas y cambiar las mac. Las direcciones mac de cada tarjeta han de ser distintas siguiendo la siguiente
codificación:

AA:AA:AA:00:00:YY

Siendo YY la numeración de la tarjeta USB que lleva en su pegatina.

Hay que renombrar las tarjetas de red para que se llamen: usbA, usbB, usbC y eth0(la de la placa base)

Al final la orden ip link show ha de mostrar algo como lo siguiente:



RENOMBRAR 


	[root@j05 pere]# ip link set usb0 down
	[root@j05 pere]# ip link set eth0 down
	[root@j05 pere]# ip link set eth1 down
	[root@j05 pere]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:b6:e0:1b brd ff:ff:ff:ff:ff:ff
		inet 192.168.3.5/16 brd 192.168.255.255 scope global dynamic enp2s0
		   valid_lft 19597sec preferred_lft 19597sec
		inet6 fe80::16da:e9ff:feb6:e01b/64 scope link 
		   valid_lft forever preferred_lft forever
	3: usb0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	4: eth0: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	5: eth1: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff



	[root@j05 pere]# ip link set usb0 name usbA
	[root@j05 pere]# ip link set eth0 name usbB
	[root@j05 pere]# ip link set eth1 name usbC
	[root@j05 pere]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:b6:e0:1b brd ff:ff:ff:ff:ff:ff
		inet 192.168.3.5/16 brd 192.168.255.255 scope global dynamic enp2s0
		   valid_lft 19441sec preferred_lft 19441sec
		inet6 fe80::16da:e9ff:feb6:e01b/64 scope link 
		   valid_lft forever preferred_lft forever
	3: usbA: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	4: usbB: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	5: usbC: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff



	[root@j05 pere]# ip link set usbA up
	[root@j05 pere]# ip link set usbB up
	[root@j05 pere]# ip link set usbC up
	[root@j05 pere]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:b6:e0:1b brd ff:ff:ff:ff:ff:ff
		inet 192.168.3.5/16 brd 192.168.255.255 scope global dynamic enp2s0
		   valid_lft 19393sec preferred_lft 19393sec
		inet6 fe80::16da:e9ff:feb6:e01b/64 scope link 
		   valid_lft forever preferred_lft forever
	3: usbA: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	4: usbB: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	5: usbC: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff

	
	
	CANVIAR MAC


	[root@j05 pere]# ip link set usbA down
	[root@j05 pere]# ip link set usbB down
	[root@j05 pere]# ip link set usbC down
	[root@j05 pere]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:b6:e0:1b brd ff:ff:ff:ff:ff:ff
		inet 192.168.3.5/16 brd 192.168.255.255 scope global dynamic enp2s0
		   valid_lft 19597sec preferred_lft 19597sec
		inet6 fe80::16da:e9ff:feb6:e01b/64 scope link 
		   valid_lft forever preferred_lft forever
	3: usbA: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	4: usbB: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
	5: eth1: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
		
		
		
	[root@j05 pere]# ip link set usbA address AA:AA:AA:00:00:06
	[root@j05 pere]# ip link set usbB address AA:AA:AA:00:00:05
	[root@j05 pere]# ip link set usbC address AA:AA:AA:00:00:04
	[root@j05 pere]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:b6:e0:1b brd ff:ff:ff:ff:ff:ff
		inet 192.168.3.5/16 brd 192.168.255.255 scope global dynamic enp2s0
		   valid_lft 19135sec preferred_lft 19135sec
		inet6 fe80::16da:e9ff:feb6:e01b/64 scope link 
		   valid_lft forever preferred_lft forever
	3: usbA: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether aa:aa:aa:00:00:06 brd ff:ff:ff:ff:ff:ff
	4: usbB: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether aa:aa:aa:00:00:05 brd ff:ff:ff:ff:ff:ff
	5: usbC: <BROADCAST,MULTICAST> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether aa:aa:aa:00:00:04 brd ff:ff:ff:ff:ff:ff



	[root@j05 pere]# ip link set usbA up
	[root@j05 pere]# ip link set usbB up
	[root@j05 pere]# ip link set usbC up
	[root@j05 pere]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:b6:e0:1b brd ff:ff:ff:ff:ff:ff
		inet 192.168.3.5/16 brd 192.168.255.255 scope global dynamic enp2s0
		   valid_lft 19074sec preferred_lft 19074sec
		inet6 fe80::16da:e9ff:feb6:e01b/64 scope link 
		   valid_lft forever preferred_lft forever
	3: usbA: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether aa:aa:aa:00:00:06 brd ff:ff:ff:ff:ff:ff
	4: usbB: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether aa:aa:aa:00:00:05 brd ff:ff:ff:ff:ff:ff
	5: usbC: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
		link/ether aa:aa:aa:00:00:04 brd ff:ff:ff:ff:ff:ff



##### 2. Conectar 3 equipos haciendo routing y saliendo a internet:

A. Configurar las ips en cada equpo según el esquema y hacer pings desde hostA a hostB y desde hostC a hostA

```
ESQUEMA CAPA 3
                       +---------+
    172.17.A.0/24    .1| ROUTER  |.1   172.17.C.0/24
   +-------------------+ HOST B  +--------------------+
        |.2        usbA|         |usbC        |.2
        |eth0          +---------+            |eth0
     +------+                              +------+
     |HOST A|                              |HOST C|
     +------+                              +------+
                            
                            

```

B. Activar el bit de forwarding y poner como default gateway al host B que hará de router entre las dos redes, listar las rutas en host B y verificar que se pueden hacer pings entre HOST A y HOST C

	echo 1 > /proc/sys/net/ipv4/ip_forward
	
	systemctl stop NetworkManager
	ip a f dev enp2s0
	
	(ordinador j05, fa de enllaç no té que canviar la ip)
	







C. Conectar eth0 del HOST B a la roseta del aula, pedir una ip dinámica con dhclient. Verificar que desde el HOST B se puede hacer ping al 8.8.8.8. 


	dhclient
	
	ping 8.8.8.8


D. Activar el enmascarmiento de ips para el tráfico saliente por eth0 y verificar que HOST A Y HOST C pueden hacer ping al 8.8.8.8


	iptables -t nat -A POSTROUTING -o enp2s0 -j MASQUERADE

E. Poner como servidor dns a 8.8.8.8 en /etc/resolv.conf en hostA y host C y verificar que se puede navegar por internet con  haciendo un wget de http://www.netfilter.org


	echo "nameserver 8.8.8.8" > /etc/resolv.conf


	[root@j05 isx46995096]# wget www.netfilter.org
	--2016-11-24 08:41:59--  http://www.netfilter.org/
	S'està resolent www.netfilter.org (www.netfilter.org)… 150.214.142.167
	S'està connectant a www.netfilter.org (www.netfilter.org)|150.214.142.167|:80… connectat.
	HTTP: s'ha enviat la petició, s'està esperant una resposta… 200 OK
	Mida: 29176 (28K) [text/html]
	S'està desant a: «index.html.2»

	index.html.2      100%[==========>]  28,49K  --.-KB/s    in 0,07s   

	2016-11-24 08:41:59 (407 KB/s) - s'ha desat «index.html.2» [29176/29176]




##### 3. Conectar 2 equipos utilizando un equipo intermedio que hace de bridge. 

En esta práctica queremos que HOST B trabaje como si fuera un switch, interconectando a host A y host C. 

A. Hacer flush de todas las ips y verificar que no queda ninguna ruta ni dirección ip asociada a ningún equipo.


	ip a f dev enp2s0
	
	
	[root@j05]# ip a f dev enp2s0
	[root@j05]# ip a
	1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
		link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
		inet 127.0.0.1/8 scope host lo
		   valid_lft forever preferred_lft forever
		inet6 ::1/128 scope host 
		   valid_lft forever preferred_lft forever
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
		link/ether 14:da:e9:99:a1:e1 brd ff:ff:ff:ff:ff:ff



B. Crear el bridge br0 y añadir usbA y usbC a ese bridge. Listar con detalle la configuración del bridge.


	brctl
	brctl show
	brctl addbr br0
	brctl addif br0 usbA
	brctl addif br0 usbC
	brctl showmacs br0
	ip link set br0 up



C. En el host A nos ponemos la ip 192.168.100.A/24 y en el equipo C la ip 192.168.100.B/24. Hacemos ping entre los dos equipos y debería de ir.


	Soc el HOST.-B
	Mitjançant l'ordre ip a a , el hostA i el HostC posaran les ip requerides per fer pings.


D. Listar en host B la tabla de relación de puertos y MACs en el bridge


[root@j05 isx46995096]# brctl showmacs br0
port no	mac addr		is local?	ageing timer
  2	14:da:e9:99:a1:e1	no		   0.06
  1	14:da:e9:b6:e9:dc	no		   0.05
  2	aa:aa:aa:00:00:04	yes		   0.00
  2	aa:aa:aa:00:00:04	yes		   0.00
  1	aa:aa:aa:00:00:06	yes		   0.00
  1	aa:aa:aa:00:00:06	yes		   0.00




##### 4. Conectar todos los equipos del aula haciendo switching.

Conseguir esta interconexión entre las filas:
```
\
                   |                     
                   +------+              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+              
 +------+ +------+        |              
                          |              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+
 +------+ +------+        |
                          |
                          +
```
Cada equipo ha de tener una ip 192.168.100.B/24

A. Lanzar fpings para verificar que todos los equipos A y C responden


[root@j05 isx46995096]# fping -g 192.168.100.4 192.168.100.6 -a -q
192.168.100.4
192.168.100.5
192.168.100.6

B. Asignar una ip al br0 del hostB y lanzar fping para verificar que todos los equipos A,B y C responden

  
  [root@j05 isx46995096]# fping -g 192.168.100.4 192.168.100.20 -a -q
192.168.100.4
192.168.100.5
192.168.100.6
192.168.100.10
192.168.100.11
192.168.100.12


C. Listar la tabla de asignación de puertos y MACs después de hacer un fping 



[root@j05 isx46995096]# brctl showmacs br0
port no	mac addr		is local?	ageing timer
  3	14:da:e9:99:a1:bc	no		   2.14
  4	14:da:e9:b6:e0:1b	yes		   0.00
  4	14:da:e9:b6:e0:1b	yes		   0.00
  1	14:da:e9:b6:e9:dc	no		  90.04
  4	40:01:c6:1a:17:01	no		  30.34
  4	40:01:c6:66:38:5e	no		  38.26
  4	40:8d:5c:b7:c8:39	no		  68.45
  4	40:8d:5c:e2:72:f7	no		  78.70
  4	40:8d:5c:e3:42:e1	no		   0.81
  4	40:8d:5c:e4:18:b7	no		  67.90
  4	40:8d:5c:e4:1d:e4	no		  54.67
  4	40:8d:5c:e4:36:fe	no		  76.45
  4	40:8d:5c:e4:37:c5	no		 104.88
  4	52:54:00:66:c3:db	no		   2.14
  4	60:a4:4c:b1:f4:ed	no		  10.15
  3	74:d0:2b:c9:f7:96	no		 227.02
  3	74:d0:2b:c9:f7:ac	no		 288.89
  4	74:d4:35:a0:80:7b	no		  24.99
  3	94:de:80:49:7d:d3	no		  82.97
  3	94:de:80:49:7e:79	no		 183.57
  2	aa:aa:aa:00:00:04	yes		   0.00
  2	aa:aa:aa:00:00:04	yes		   0.00
  3	aa:aa:aa:00:00:05	yes		   0.00
  3	aa:aa:aa:00:00:05	yes		   0.00
  1	aa:aa:aa:00:00:06	yes		   0.00
  1	aa:aa:aa:00:00:06	yes		   0.00
  

##### 5. Conectar todos los equipos del aula haciendo routing.

```
\
                         +
                         |
                         |
                         |
                     +---+-----+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2            |         |          .2|
      |              +---------+            |
   +--+---+              |.1             +--+---+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |172.16.XX.0/24
                         |
                         |
                         |
                     usbB|.2
                     +---------+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2        usbA|         |usbC        |.2
      |eth0          +---------+            |eth0
   +------+          eth0|.1             +------+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |
                         +
```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden

##### 6. Conectar todos los equipos del aula haciendo switching y routing.

Seguir este esquema


```
                                       172.16.0.0/24
           +------------------------------------------------------------------------------+
                        |.XX                                                     |.XX
                        |                                                        |
                        |br0(usbB,eth0)                                          |br0(usbB,eth0)
                    +---------+                                              +---------+
 172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24        172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
+-------------------+ HOST B  +--------------------+     +-------------------+ HOST B  +--------------------+
     |.2        usbA|         |usbC        |.2                |.2        usbA|         |usbC        |.2
     |eth0          +---------+            |eth0              |eth0          +---------+            |eth0
  +------+                              +------+           +------+                              +------+
  |HOST A|                              |HOST C|           |HOST A|                              |HOST C|
  +------+                              +------+           +------+                              +------+

```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden
