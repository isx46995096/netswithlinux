File "flash:c2950-c3h2s-mz.120-5.3.WC.1.bin" uncompressed and installed, entry point: 0x80010000
executing...

              Restricted Rights Legend

Use, duplication, or disclosure by the Government is
subject to restrictions as set forth in subparagraph
(c) of the Commercial Computer Software - Restricted
Rights clause at FAR sec. 52.227-19 and subparagraph
(c) (1) (ii) of the Rights in Technical Data and Computer
Software clause at DFARS sec. 252.227-7013.

           cisco Systems, Inc.
           170 West Tasman Drive
           San Jose, California 95134-1706



Cisco Internetwork Operating System Software 
IOS (tm) C2950 Software (C2950-C3H2S-M), Version 12.0(5.3)WC(1), MAINTENANCE INTERIM SOFTWARE
Copyright (c) 1986-2001 by cisco Systems, Inc.
Compiled Mon 30-Apr-01 07:56 by devgoyal
Image text-base: 0x80010000, data-base: 0x8031A000


Initializing flashfs...
flashfs[1]: 161 files, 3 directories
flashfs[1]: 0 orphaned files, 0 orphaned directories
flashfs[1]: Total bytes: 7741440
flashfs[1]: Bytes used: 2960896
flashfs[1]: Bytes available: 4780544
flashfs[1]: flashfs fsck took 6 seconds.
flashfs[1]: Initialization complete.
Done initializing flashfs.
C2950 POST: System Board Test : Passed
C2950 POST: Ethernet Controller Test : Passed
C2950 POST: MII TEST : Passed

cisco WS-C2950-12 (RC32300) processor (revision B0) with 22260K bytes of memory.
Processor board ID FOC0546Z06B
Last reset from system-reset

Processor is running Enterprise Edition Software
Cluster command switch capable
Cluster member switch capable
12 FastEthernet/IEEE 802.3 interface(s)

32K bytes of flash-simulated non-volatile configuration memory.
Base ethernet MAC Address: 00:07:EB:CB:56:80
Motherboard assembly number: 73-5782-08
Power supply part number: NONE
Motherboard serial number: FOC05460371
Power supply serial number: DAB05411P8V
Model revision number: B0
Model number: WS-C2950-12
System serial number: FOC0546Z06B
C2950 INIT: Complete

00:00:17: %SYS-5-RESTART: System restarted --
Cisco Internetwork Operating System Software 
IOS (tm) C2950 Software (C2950-C3H2S-M), Version 12.0(5.3)WC(1), MAINTENANCE INTERIM SOFTWARE
Copyright (c) 1986-2001 by cisco Systems, Inc.
Compiled Mon 30-Apr-01 07:56 by devgoyal
