Exercicis de Subxarxes
Instruccions generals:

    Alerta, feu tots els càlculs sense calculadora!
    Si necessiteu fer càlculs en un full apart, si us plau, indiqueu-m'ho en cada exercici i entregueu-me aquest full.
    En total hi ha 5 exercicis (reviseu totes les pàgines)

1. Donades les següents adreces IP, completa la següent taula (indica les adreces de xarxa i de broadcast tant en format decimal com en binari).

      IP         |	Adreça de Xarxa |	Adreça de Broadcast Extern |	Rang IPs per a Dispositius
      ---|---|---|---
172.16.4.23/25  |	172.16.4.0/25 	 |    172.16.4.127         |       126 disp. -172.16.4.1 a 172.16.4.126
174.187.55.6/23 |	174.187.54.0/23 |	174.187.55.255 	        |     510 disp. 174.187.54.1 a 174.187.55.254
10.0.25.253/18  |	10.0.0.0/18 	  |   10.0.63.255 	         |    16382 disp. - 10.0.0.1 a 10.0.63.254
209.165.201.30/27| 	209.165.201.0/27 |	209.165.201.31 	          |   30 disp. - 209.165.201.1 a 209.165.201.30




2. Donada la xarxa 172.28.0.0/16, calcula la màscara de xarxa que necessites per poder fer 80 subxarxes. Tingues en compte que cada subxarxa ha de tenir capacitat per a 400 dispositius. Indica la màscara de xarxa calculada tant en format decimal com en binari.

La màscara per  80 subxarxes : 255.255.254.0 = /23 
Aagafem 7 bits prestats: 2⁷=128 
 32b-23b= 9 bits per host -->  2⁹-2 = 510 disp. (necesitem 400)




3. Donada la xarxa 10.192.0.0

a) Calcula la màscara de xarxa que necessites per poder adreçar 1500 dispositius. Indica-la tant en format decimal com en binari.

11111111.11111111.11111000.00000000 = 255.255.248.0 /21  (2.048-2 -->2.046 hosts)


b) Si fixem l'adreça de la xarxa mare a la 10.192.0.0/10, quantes subxarxes de 1500 dispositius podrem fer?

32b-10b (--->/10)= 22bits 

2²²= 4194302 dispositivos totales
4194302/1500= 2796 subxarxes 


c) Si ara considerem que l'adreça de xarxa mare és la 10.192.0.0 amb la màscara de xarxa calculada a l'apartat (a), calcula la nova màscara de xarxa que permeti fer 3 subxarxes de 500 dispositius cadascuna. Indica-la tant en format decimal com en binari.


Per 3 subxarxes agafem 2b prestats 2^2= 4 subxarxes
Per 500 dispositius= 32b-23b=9 ---> 2^9-2= 510 disp.
La mascara nova= 255.255.254.0 ---> 1111111.11111111.11111110.0000000




4. Donada la xarxa 10.128.0.0 i sabent que s'ha d'adreçar un total de 3250 dispositius

a) Calcula la màscara de xarxa per crear una adreça de xarxa mare que permeti adreçar tots els dispositius indicats. Indica-la tant en format decimal com en binari.


Per 3250 disp= 12b prestats ---> 2^12-2= 4094 disp.
32b-12b= 20 bits ---> La mascara serà /20= 1111111.1111111.11110000.00000000 -->255.255.240.0



b) Calcula la nova màscara de xarxa que permeti fer 5 subxarxes amb 650 dispositius cadascuna d'elles. Indica-la tant en format decimal com en binari.

Per 5 subxaraxes--> 2^3= 8 subxarxes i que tingui 650 dispositius ---> 32b-23b =9---> 2^9-s=510 disp.
No es pot fer 5 subxarxes i que cadascuna tingui 650 disp. (Màxim seria de 510 disp.)



c) Comprova que, realment, cada subxarxa pot adreçar els 650 dispositius demanats. En cas que no sigui així, quina és la capacitat màxima de dispositius de cadascuna d'aquestes xarxes? Indica la fórmula que fas anar per calcular aquesta dada.

No pot adreçar 650 dipositius, la fórmula utilitzada:

 Bits totals (32b) - Bits de la màscara de xarxa = bits de host --->2^* de bits de host - 2 =dispositius totals.



d) A partir de la xarxa mare que has obtingut amb la màscara de xarxa calculada a l'apartat (a), podem fer dues subxarxes amb 1625 dispositius cadascuna d'elles? Indica els càlculs que necessites fer per raonar la teva resposta.

Per a 2 subxarxes ens caldran 2b, la màscar per fer-la quedarà en una /21--->
32b-21b=11 ---> 2^11-2= 2046 dispositius.

Per tant,si que es poden fer


5. Donada l'adreça de xarxa mare (AX mare) 172.16.0.0/12, s'han de calcular 6 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 6 subxarxes?

Necessitarem 3 bits---> 2³ = 8 (8 subxarxes) i així obtenim una màscara nova /15 


b) Dóna la nova MX, tant en format decimal com en binari.

 255.254.0.0 = /15.
En binari : 11111111.11111110.00000000.00000000
En decimal: 255.254.0.0

c) Per cada subxarxa nova que has de crear, indica i. L'adreça de xarxa, en format decimal i en binari ii. L'adreça de broadcast extern, en format decimal i en binari iii. El rang d'IPs per a dispositius


Nom 	|         Adreça de Xarxa 	                             |          Adreça de Broadcast Extern 	                  | Rang IPs
   ---|---|---|---
Xarxa 1 |	172.16.0.0/15 -- 10101100.00010000.00000000.00000000 |	172.17.255.255 -- 10101100.00010001.11111111.11111111 |	1 al 254
Xarxa 2 |	172.18.0.0/15 -- 10101100.00010010.00000000.00000000 |	172.19.255.255 -- 10101100.00010011.11111111.11111111 |	1 al 254
Xarxa 3 |	172.20.0.0/15 -- 10101100.00010100.00000000.00000000 |	172.21.255.255 -- 10101100.00010101.11111111.11111111 |	1 al 254
Xarxa 4 |	172.22.0.0/15 -- 10101100.00010110.00000000.00000000 |	172.23.255.255 -- 10101100.00010111.11111111.11111111 |	1 al 254
Xarxa 5 |	172.24.0.0/15 -- 10101100.00011000.00000000.00000000 |	172.25.255.255 -- 10101100.00011001.11111111.11111111 |	1 al 254
Xarxa 6 |	172.26.0.0/15 -- 10101100.00011010.00000000.00000000 |	172.27.255.255 -- 10101100.00011011.11111111.11111111 |	1 al 254


d) Tenint en compte el número de bits que has indicat en l'apartat a), quantes subxarxes podríem fer, realment? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.


2^3=8b prestats = 8 subxarxes que necessitem

Fórmula: 2^n bits (n = 0 fins 7)



e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

Es pot obenir el resultat fent : 32b - 15b = 17b--->
2¹⁷ - 2 = 131070 dispositius.

Fórmula= Bits totals (32) - Bits de la màscara = bits de host ---> 
2^* de bits de host que hem obtingut - 2 =dispositius totals. 


6. Donada l'adreça de xarxa mare (AX mare) 192.168.1.0/24, s'han de calcular 4 subxarxes.

a) Quants bits necessites per ampliar la Màscara de Xarxa (MX) per tal de poder fer aquestes 4 subxarxes?

Es necessiten 2 bits (2² = 4) per a 4 subxarxes diferents. 
La màscara cambia a /26



b) Dóna la nova MX, tant en format decimal com en binari.

La nova màscara: 255.255.255.192 = /26.
En binari : 11111111.11111111.11111111.11000000
En decimal: 255.255.255.192

c) Per cada subxarxa nova que has de crear, indica i. L'adreça de xarxa, en format decimal i en binari ii. L'adreça de broadcast extern, en format decimal i en binari iii. El rang d'IPs per a dispositius

Nom 	 |   Adreça de Xarxa                                     	|  Adreça de Broadcast Extern                          |Rang IPs
---|---|---|---
Xarxa 1  | 	192.168.1.0/26 -- 11000000.10101000.00000001.00000000 	|  192.168.1.63 - 11000000.10101000.00000001.00111111  |	1 al 62
Xarxa 2  |	192.168.1.64/26 -- 11000000.10101000.00000001.01000000 	|   192.168.1.127- 11000000.10101000.00000001.01111111 	|65 al 126
Xarxa 3  |	192.168.1.128/26 -- 11000000.10101000.00000001.10000000 |   192.168.1.191- 11000000.10101000.00000001.10111111 	|129 al 190
Xarxa 4  |	192.168.1.192/26 -- 11000000.10101000.00000001.11000000 |	192.168.1.255 -11000000.10101000.00000001.11111111  |	193 al 254


d) Tenint en compte el número de bits que has indicat en l'apartat a), podríem fer més de 4 subxarxes? Dóna'n la fórmula que has utilitzat per respondre a la pregunta.

Amb 2 bits es poden fer  4 subxarxes:
2² = 4---> 4 subxarxes diferents. 
Per a cada subxarxa necessitem 1 bit per tant amb 2² bits nomès hi han 4 combinacions. 


Fórmula: 2^n bits (n pot ser del 0 al 7)


e) Segons la MX que has calculat als apartats a) i b), quants dispositius pot tenir cada subxarxa? Dóna'n la fórmula que s'utilitza per calcular aquesta dada.

Per calcular-la fem :32b - 26b = 6--->
2⁶ - 2 = 62 dispositius.

Fórmula: Bits totals (32) - Bits de la màscara = bits de host----> 
 2^* de bits de host - 2 i obtenim el nombre de dispositius totals.
